# WEATHER APP PROJECT

# Stack:
React Native
    https://facebook.github.io/react-native/
External API: OpenWeather Current weather and 5 day forecast APIs. https://openweathermap.org/api


# Functionality: 
Single-screen app shows weather information now, scroll the middle of the screen for the next 36 hour weather, and a summary for the next 4 days. 
Search icon (bottom left) allows user to search for weather by city name. 
Location icon (bottom right) takes user back to weather from based on their current location. 
My first React Native app built in 2 days.


# Deployment:
Go to my Expo Snack
    https://snack.expo.io/@pandorajk/weatherapp
Either:
Run the app via the screen generated on the right hand side of the screen.
Or:
Download the expo app, click 'Run' on the project - and scan the QR code (which opens you Expo app for you) to see the app.


# Screenshot: 

https://www.dropbox.com/s/zvgqv4vnlhr2bhd/weatherApp-project.png?dl=0


# Set up and Installation: 
-git clone git clone https://gitlab.com/Pandorajk/BCS_weatherApp_project
-Install React Native and ensure the following expo/react packages are downloaded with npm install: 
    "expo": "^35.0.0",
    "expo-linear-gradient": "^7.0.0",
    "react": "16.8.3",
    "react-dom": "16.8.3",
  * "react-native": "https://github.com/expo/react-native/archive/sdk-35.0.0.tar.gz",
    "react-native-swipeout": "^2.3.6",
    "react-native-web": "^0.11.7"
-npm install expo-cli --global
-On mobile: use command expo start in the command line to launch the app's QR code. Install Expo App from the app store. Scan the QR code with your device to see the app on IOS.


# Commit history: 
1. Initial commit - (08/10/2019)
2. readme and UI update
3. Input added to search for city
4. City search input complete, icons added, and background changes according to weather. Still to complete : 5 day weather info & UI
5. Next 12 hours data added, UI updated. 5 day data hardcoded and to be coded dynamically tomorrow.
6. 5 day weather now working. App mostly complete. 1x bug: sometimes API doesn't send enough data for 4th day.
7. Bug fixed. Code refactored. Could change background colors for better UI?


# Contact:
Pandora Jane Knocker - pjknocker@yahoo.com

