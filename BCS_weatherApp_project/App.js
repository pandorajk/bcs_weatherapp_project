import React from 'react';
import Constants from 'expo-constants';

import { StyleSheet, Text, View, Image, Button, TextInput, TouchableHighlight, ScrollView, Modal } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { IconObj } from './components/IconObj';

export default class App extends React.Component {

  state = {
    //LOADING:
    loaded: false,
    //DATE AND TIME-DIFFERENCE: 
    date : '',
    timeDiffInHours : '',
    //LOCATION:
    latitude: '',
    longitude: '',
    //WEATHER:
    weather: '',
    allIcons: {},
    nowIcon: 'n',
    isNight : false,
    //CHANGE CITY:
    changeCity: '',
    //CLIENT MESSAGE: 
    message: '', 
    //FORECASTINFO: 
    threeHourly: [],
    fourDay: [], 
    //MODAL:
    modalVisible: false
  };

//==========  MOUNT APP: ========== 
  componentDidMount() {
    this.getDate();
    this.getIcon();
    this.getLocation();
  }

  getDate = () => {
    var date = new Date().toDateString();
    this.setState({date})
 } 
  
  getIcon = () => {
    this.setState({ allIcons: IconObj });
  };

  getLocation = () => {
    navigator.geolocation.getCurrentPosition(position => {
      this.setState({ latitude: position.coords.latitude,  longitude: position.coords.longitude  });
      // ASUNCION, PARAGUAY - for testing;
      // this.setState({ latitude: -25.30066});  
      // this.setState({ longitude:-57.63591});
      this.getWeather(this.state.latitude, this.state.longitude);
    });
  };

  getWeather = (latitude, longitude ) => {
    // var api_key = '16909a97489bed275d13dbdea4e01f59' //bcs api
    var api_key = 'aaa77b419ca754585b3536225d75992d';  //my api
    var url = `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&APPID=${api_key}`;
    fetch(url)
      .then(res1 => res1.json())
      .then(res2 => {
          console.log('weather NOW res 2', res2)
          this.setState({ weather: res2, loaded: false })
          this.setIcon(this.state.weather)
          this.get5DayWeather(this.state.latitude, this.state.longitude); 
      })
      .catch(error => console.log(error));
  };

  setIcon = weather => {
    var isNight = weather.weather[0].icon.includes('n')
    isNight
    ? this.setState({ nowIcon: weather.weather[0].icon, isNight : true })
    : this.setState({ nowIcon: weather.weather[0].icon, isNight : false })
  };

get5DayWeather = (latitude, longitude) => {
  // var api_key = '16909a97489bed275d13dbdea4e01f59' //bcs api
  var api_key = 'aaa77b419ca754585b3536225d75992d';  //my api
  var url = `http://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&units=metric&APPID=${api_key}`;
  fetch(url)
    .then(res1 => res1.json())
    .then(res2 => {
        console.log('weather FORECAST res2', res2)

        let timezone = res2.city.timezone / 60 / 60
        let timeDiffInHours = timezone - 2
        this.setState({timeDiffInHours})

        let threeHourly = []
        let bool = false
        let resCopy = [...res2.list]
        //Map through the 'res2.list' Array which always has 40 objects
        res2.list.map((e,i)=>{
            //Cacluate hour with correct time difference. 
            let hour = e.dt_txt.slice(11, -3)

            let shortHour = e.dt_txt.slice(11, -6)
            let updatedHour = parseInt(shortHour) + this.state.timeDiffInHours
            if (updatedHour < 0) {
              updatedHour = 24 + updatedHour
             }
            if (updatedHour < 10) {
              updatedHour = "0" + updatedHour
             }
            let newHour = updatedHour + ":00"
            
        //1. 
        //Populate the threeHourly Array - up to 12 objects - with hour, icon, weatherIcon and temp. 
            if ( threeHourly.length < 12 ) {
              threeHourly.push({ hour : newHour, icon : e.weather[0].icon, weather : e.weather[0].main, temp : Math.round(e.main.temp)})
            }
        this.setState({threeHourly, loaded : false})
        //2. 
        //Create the data set for fourDay weather ->   Date, Month, Weather, Max and Min x4
        //2.a Cut the A COPY OF 'res2.list' Array down to 32 objects starting from TOMORROW.   (= 8objects per day for 4 days)

              if ( hour == '00:00' && bool === false ) {
                resCopy.splice(0, 8)
                bool = true
                resCopy.splice(32)
              }
              if ( hour == '03:00' && bool === false ) {
                resCopy.splice(0, 7)
                bool = true
                resCopy.splice(32)
              }
              if ( hour == '06:00' && bool === false ) {
                resCopy.splice(0, 6)
                bool = true
                resCopy.splice(32)
              }
              if ( hour == '09:00' && bool === false ) {
                resCopy.splice(0, 5)
                bool = true
                resCopy.splice(32)
              }
              if ( hour == '12:00' && bool === false ) {
                resCopy.splice(0, 4)
                bool = true
                resCopy.splice(32)
              }
              if ( hour == '15:00' && bool === false ) {
                resCopy.splice(0, 3)
                bool = true
                resCopy.splice(32)
              }
              if ( hour == '18:00' && bool === false ) {
                resCopy.splice(0, 2)
                bool = true
                resCopy.splice(32)
              }
              if ( hour == '21:00' && bool === false ) {
                resCopy.splice(0, 1)
                bool = true
                resCopy.splice(32)
              }
        })
        //2b. Map through the res2.list Array (now with 32 objects) and divide into 4 arrays of 8 objects pushed into a fourDayPrep array.
        let fourDayPrep = []
        for (var i = 0 ; i < 4 ; i++) {
          var dayData = resCopy.splice(0, 8)
          fourDayPrep.push(dayData)
        }
        // })
        //2c. Map through fourDayPrep Array and for each element find Date, Month, Max and Min and weatherIcon at midday. Create and object with this data and push the object to fourDay Array. 
        let fourDay = []
        fourDayPrep.map((ele,idx)=>{
          var date = ele[0].dt_txt.slice(8, -9);
          var month = ele[0].dt_txt.slice(5, -12);
          var icon = ele[4].weather[0].icon;
          var max = Math.round(ele[0].main.temp_max);
          var min = Math.round(ele[0].main.temp_min);
            ele.map((e,i)=>{
                if (e.main.temp_max > max) {
                  max = Math.round(e.main.temp_max)
                }
                if (e.main.temp_min < min) {
                  min = Math.round(e.main.temp_min)
                }
            })
            fourDay.push({date, month, icon, max, min})
        })
        //2d. Set the state with the fourDay array
          this.setState({fourDay, loaded : true, modalVisible: false })
    })
    .catch(error => console.log(error));
}

//========== RE-SET WHEN CITY CHANGED: ========== 

  handlePress = async () => {
    var city = this.state.changeCity
   // var api_key = '16909a97489bed275d13dbdea4e01f59' //bcs api
    var api_key = 'aaa77b419ca754585b3536225d75992d';  //my api
    var url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&APPID=${api_key}`
    fetch(url)
    .then(res1 => res1.json())
    .then(res2 => {
        console.log('res 2 when city changed: ->', res2)
        res2.cod == '404'
        ? this.setState({ message: 'City not found' })
        : this.setState({ latitude: res2.coord.lat,  longitude: res2.coord.lon }), this.getWeather(res2.coord.lat, res2.coord.lon)
    })
    .catch(error => console.log(error));
  };

//========== MODAL: ========== 
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    // console.log('loaded?', this.state.loaded)
    // console.log('before render - threeHourly', this.state.threeHourly)
    // console.log('before render - fourDay', this.state.fourDay)
    return !this.state.loaded ? (
      <View style={styles.sec1}>
        <Text>Loading...</Text>
      </View>
    ) : (
      <LinearGradient
        colors={ weatherBackground[this.state.nowIcon]}
        style={{ flex: 1 }}>
{/* SEC 1 - NOW */}
          <View style={styles.sec1}> 
            <Text style={styles.textHeader}>
              {this.state.weather.name}, {this.state.weather.sys.country}
            </Text>
            <Text style={styles.textMediumCenter}>
            {this.state.date}
            </Text>
            <View style={styles.todayContainer}>
              <View style={styles.todayFirst}>
                  <Image source={IconObj[this.state.nowIcon]} style={{width: 90, height:90}} />
              </View>
              <View style={styles.todaySecond}>
                <Text style={styles.textxlrg}>
                {Math.round(this.state.weather.main.temp)}° 
                </Text>
              </View>
              <View style={styles.todayThird}>
              <Text style={styles.text}>
                ▲ {this.state.weather.main.temp_max}° 
                </Text>
                <Text style={styles.text}>
                ▼ {this.state.weather.main.temp_min}° 
                </Text>
                <Text style={styles.text}>
                    Humidity {this.state.weather.main.humidity}% 
                </Text>
              </View>
            </View>
            {/* <Text style={styles.textMediumCenter}>
                    {this.state.weather.weather[0].main} - expect: {this.state.weather.weather[0].description}
            </Text> */}
          </View>
{/* SEC 2 - 3 HOURLY FORECAST  */}
        <View style={styles.sec2}>
          <ScrollView>
          {this.state.threeHourly.map((e,i)=>{
            return <View style={styles.threeHourlyRow} key={i}>
            <Text style={styles.textMedium}>{ i === 0 ? 'Later ' : e.hour}</Text>
            <Image source={IconObj[e.icon]} style={{width: 20, height:20}} />
            <Text style={styles.textMedium}>{e.weather}</Text>
            <Text style={styles.textMedium}>{e.temp}°</Text>
            </View>
          })}
        </ScrollView>
        </View>
{/* SEC 3 - NEXT 5 DAYS */}
        <View style={styles.sec3}>
        <Text style={styles.text}>4 Day Forecast</Text>
          <View style={styles.fiveDayRow}>
            <View style={styles.fiveDayBox}>
                <View>
                  <Text style={styles.text}>{this.state.fourDay[0].date}/{this.state.fourDay[0].month}</Text>
                </View>
                <View>
                  <Image source={IconObj[this.state.fourDay[0].icon]} style={{width: 35, height:35}} />
                </View>
                <View>
                  <Text style={styles.text}>▲{this.state.fourDay[0].max}°</Text>
                  <Text style={styles.text}>▼{this.state.fourDay[0].min}°</Text>
                </View>
              </View>
              <View style={styles.fiveDayBox}>
                <View>
                  <Text style={styles.text}>{this.state.fourDay[1].date}/{this.state.fourDay[1].month}</Text>
                </View>
                <View>
                  <Image source={IconObj[this.state.fourDay[1].icon]} style={{width: 35, height:35}} />
                </View>
                <View>
                  <Text style={styles.text}>▲{this.state.fourDay[1].max}°</Text>
                  <Text style={styles.text}>▼{this.state.fourDay[1].min}°</Text>
                </View>
              </View>
             </View>
          <View style={styles.fiveDayRow}>
            <View style={styles.fiveDayBox}>
               <View>
                  <Text style={styles.text}>{this.state.fourDay[2].date}/{this.state.fourDay[2].month}</Text>
                </View>
                <View>
                  <Image source={IconObj[this.state.fourDay[2].icon]} style={{width: 35, height:35}} />
                </View>
                <View>
                  <Text style={styles.text}>▲{this.state.fourDay[2].max}°</Text>
                  <Text style={styles.text}>▼{this.state.fourDay[2].min}°</Text>
                </View>
              </View>
              <View style={styles.fiveDayBox}>
                <View>
                  <Text style={styles.text}>{this.state.fourDay[3].date}/{this.state.fourDay[3].month}</Text>
                </View>
                <View>
                  <Image source={IconObj[this.state.fourDay[3].icon]} style={{width: 35, height:35}} />
                </View>
                <View>
                  <Text style={styles.text}>▲{this.state.fourDay[3].max}°</Text>
                  <Text style={styles.text}>▼{this.state.fourDay[3].min}°</Text>
                </View>
             </View>
          </View>
      </View>
{/* SEC 4 - SEARCH/LOCATE */}

          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            style={styles.modal}
            onBackdropPress={this.closeModal}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.modal}>
              <View style={styles.searchContainer}>
                <View style={styles.formContainer}>
                    <TextInput
                      style={styles.inputStyle}
                      onChangeText={text => this.setState({ changeCity: text })}
                    />
                </View>
                <View>
                    <Button
                      onPress={() => this.handlePress()}
                      title="Go!"
                      color="black"
                    />
                    <Text style={styles.text}>  
                    {/* this will be deleted when I have the google place auto complete */}
                    {this.state.message}
                    </Text> 
                </View>
              </View>
            </View>
          </Modal>

          <View style={styles.sec4}>
          {/* <View style={styles.temp}> */}
              <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                  <Image source={require('./assets/icons/search.png')} style={{width: 45, height:45}} />
              </TouchableHighlight>

              <TouchableHighlight onPress={() => this.getLocation()}  >
                <Image source={require('./assets/icons/location.png')} style={{width: 30, height:45}} />
              </TouchableHighlight>
          {/* </View> */}
        </View>
{/* END OF SECTIONS */}
      </LinearGradient>
    )
  }
};

const weatherBackground = {
  '01d': ['#ff7b0f', '#edd30c', '#ffae00', '#ffae00'], 
  '02d': ['#3588ab', '#4fe8e8', '#70adc2', '#70adc2'],
  '03d': ['#596880', '#95dee8', '#8c8e8f', '#8c8e8f'], 
  '04d': ['#50565c', '#abb5ba', '#35424f', '#35424f'], 
  '09d': ['#4f5c63', '#94adb0', '#85c9d6', '#85c9d6'], 
  '10d': ['#686c70', '#839ea8', '#51585c', '#51585c'], //  full rain to do onwards (review 04 the grey one? make darker at the top) - 10.10.19
  '11d': ['#2e363b', '#d9d9d9', '#494b4d', '#494b4d'],
  '13d': ['#747d7d', '#c3d6d6', '#cfcfcf', '#cfcfcf'],
  '50d': ['#7a7a7a', '#1d2024', '#666b70', '#666b70'],
  'n'  : ['#151617', '#444a4f', '#1d2024', '#1d2024'],
  '01n': ['#151617', '#444a4f', '#1d2024', '#1d2024'],
  '02n': ['#151617', '#444a4f', '#1d2024', '#1d2024'],
  '03n': ['#151617', '#444a4f', '#1d2024', '#1d2024'], 
  '04n': ['#151617', '#444a4f', '#1d2024', '#1d2024'],
  '09n': ['#151617', '#444a4f', '#1d2024', '#1d2024'], 
  '10n': ['#151617', '#444a4f', '#1d2024', '#1d2024'],
  '11n': ['#151617', '#444a4f', '#1d2024', '#1d2024'], 
  '13n': ['#151617', '#444a4f', '#1d2024', '#1d2024'],
  '50n': ['#151617', '#444a4f', '#1d2024', '#1d2024'], 
};

const styles = StyleSheet.create({
  sec1 : {
    flex : 4,
    marginTop: 50,
  },
  sec2 : {
    flex : 2.5
  },
  sec3 : {
    flex : 3,
    justifyContent: 'center',
    marginTop: 30,
    marginLeft: 20,
    marginRight: 18,
    marginBottom: 10,
  },
  sec4 : {
    flex : 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  textHeader: {
    fontSize: 25,
    color: 'white',
    textAlign: 'center',
    marginBottom : 5, 
    fontFamily: 'AppleSDGothicNeo-Thin'
  },
  textMediumCenter: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center', 
    fontFamily: 'AppleSDGothicNeo-Thin'
  },
  textMedium: {
    fontSize: 20,
    color: 'white',
    fontFamily: 'AppleSDGothicNeo-Thin'
  },
  todayContainer : {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20
  },
  todayFirst : {
    justifyContent : 'center',
  },
  todaySecond : {
    justifyContent : 'center',
  },
  todayThird : {
    justifyContent : 'center',
  },
  text: {
    fontSize: 15,
    color: 'white',
    fontFamily: 'AppleSDGothicNeo-Thin'
  },
  threeHourlyRow : {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginLeft: 20,
    marginRight: 20
  },
  textlrg: {
    fontSize: 25,
    color: 'white',
    fontFamily: 'AppleSDGothicNeo-Thin'
  },
  textxlrg: {
    fontSize: 125,
    color: 'white',
    fontFamily: 'AppleSDGothicNeo-Thin'
  },
  inputStyle: {
    borderWidth: 1,
    borderColor: 'black',
    color: 'black'
  }, 
  fiveDayRow : {
    width: '100%',
    flexDirection: 'row', 
    justifyContent: 'space-around',
    paddingTop : 5,
    paddingBottom : 5
  },
  fiveDayBox : {
    flexDirection: 'row', 
    justifyContent: 'space-between',
    width: '48%',
    padding: 10,
    borderWidth: 1,
    borderColor: 'white',
  },
  searchContainer : {
    backgroundColor : 'white',
    height : 100, 
    padding : 20
  }, 
  formContainer: {
    width: '100%',
  },
  modal: {
    marginTop: 250,
    marginRight: 20,
    marginLeft : 20
  }
});


